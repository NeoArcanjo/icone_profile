defmodule IconeProfile do
  @moduledoc """
  Documentation for IconeProfile.
  """

  alias IconeProfile.Imagem

  def main(input) do
    input
    |> hash_input
    |> create_color
    |> table_build
    |> rem_odd
    |> pixel_build
    |> draw
    |> save(input)
  end

  def save(image, input) do
    File.write("priv/static/images/#{input}.png", image)
  end

  def draw(%Imagem{color: color, pixel_map: pixel_map}) do
    image = :egd.create(250, 250)
    fill = :egd.color(color)

    Enum.each(pixel_map, fn {start, stop} ->
      :egd.filledRectangle(image, start, stop, fill)
    end)

    :egd.render(image)
  end

  def pixel_build(%Imagem{grid: grid} = imagem) do
    pixel_map =
      Enum.map(grid, fn {_valor, indice} ->
        h = rem(indice, 5) * 50
        v = div(indice, 5) * 50
        top_left = {h, v}
        bottom_right = {h + 50, v + 50}
        {top_left, bottom_right}
      end)

    %Imagem{imagem | pixel_map: pixel_map}
  end

  def create_color(%Imagem{hex: [r, g, b | _tail]} = imagem) do
    %Imagem{imagem | color: {r, g, b}}
  end

  def table_build(%Imagem{hex: hex} = imagem) do
    grid =
      hex
      |> Enum.chunk_every(3, 3, :discard)
      |> Enum.map(&mirror/1)
      |> List.flatten()
      |> Enum.with_index()

    %Imagem{imagem | grid: grid}
  end

  def mirror(row) do
    [fst, sec | _tail] = row
    row ++ [sec, fst]
  end

  def rem_odd(%Imagem{grid: grid} = imagem) do
    grid = Enum.filter(grid, fn {valor, _indice} -> rem(valor, 2) == 0 end)
    %Imagem{imagem | grid: grid}
  end

  def hash_input(input) do
    hex =
      :crypto.hash(:md5, input)
      |> :binary.bin_to_list()

    %Imagem{hex: hex}
  end
end
